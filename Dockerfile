FROM php:7.3-fpm-stretch

RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng-dev \
        libxml2-dev \
        libexpat1-dev \
        libpq-dev \
        libzip-dev

RUN pecl install mcrypt-1.0.2
RUN docker-php-ext-enable mcrypt

RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/
RUN docker-php-ext-install -j$(nproc) gd

RUN CFLAGS="-I/usr/src/php" docker-php-ext-install -j$(nproc) iconv mbstring xml xmlreader xmlwriter xmlrpc pdo pdo_mysql pdo_pgsql zip pgsql

#     pecl install xdebug-2.5.5 \
#     docker-php-ext-enable xdebug
